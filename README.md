OctoPrint Docker container, Thanks to : http://octoprint.org/


this uses https://hub.docker.com/r/rbartl/docker-octoprint/ and adds some
plugins.

* OctoPrint-DisplayZ
* OctoPrint-DetailedProgress
* OctoPrint-MQTT 
* OctoPrint-TouchUI
* OctoPrint-DisplayProgress
* OctoPrint-NavbarTemp

run with
---------

docker run -d -p 5001:5000 -p 8088:8088 --device=/dev/video0 \
--device=/dev/serial/by-id/usb-FTDI_FT232R_USB_UART_A403M08H-if00-port0:/dev/ttyUSB0 /
-v /octo/data:/data \
rbartl/docker-octoprint-plugins




